//
//  IntegerDeque.hpp
//  STALab1
//
//  Created by trung on 22.06.2018.
//  Copyright © 2018 trung. All rights reserved.
//

#ifndef IntegerDeque_hpp
#define IntegerDeque_hpp

#include <stdio.h>

/*****************************************************************************/

struct IntegerDeque {
    //    ​ Количество ячеек в массиве-директории
    int m_directorySize;
    //    ​ Количество ячеек в каждом блоке данных
    int m_blockSize;
    //    ​ Массив-директория: указатели на блоки данных
    int ** m_pDirectory;
    //    ​ Индексы верхней и нижней позиции из занятых ячеек в директории
    int m_frontBlockIndex, m_backBlockIndex;
    //    ​ Количество занятых элементов в первом и последнем блоках
    int m_frontUsed, m_backUsed;
};

/*****************************************************************************/

void IntegerDequeInit(IntegerDeque & _queue);
void IntegerDequeDestroy(IntegerDeque & _queue);
void IntegerDequeClear(IntegerDeque & _queue);
bool IntegerDequeIsEmpty(const IntegerDeque & _queue);
void IntegerDequePushFront(IntegerDeque & _queue, const int value);
void IntegerDequePopFront(IntegerDeque & _queue);
int  IntegerDequeFrontValue(const IntegerDeque & _queue);
void IntegerDequePushBack(IntegerDeque & _queue, const int value);
void IntegerDequePopBack(IntegerDeque & _queue);
int IntegerDequeBackValue(const IntegerDeque & _queue);

#endif /* IntegerDeque_hpp */
