//
//  TestCase.hpp
//  STALab1
//
//  Created by trung on 22.06.2018.
//  Copyright © 2018 trung. All rights reserved.
//

#ifndef TestCase_hpp
#define TestCase_hpp

/*****************************************************************************/

#include <cassert>
#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

/*****************************************************************************/

typedef void ( * TestProcedure )();

/*****************************************************************************/

class TestsRunner
{
    
    /*-----------------------------------------------------------------*/
    
public:
    
    /*-----------------------------------------------------------------*/
    
    void addTest ( std::string const & _tpName, TestProcedure _tp )
    {
        m_testProcedures.push_back( std::make_pair( _tpName, _tp ) );
    }
    
    /*-----------------------------------------------------------------*/
    
    void runTests ()
    {
        assert( ! m_testProcedures.empty() );
        
        std::cout << "Running " << m_testProcedures.size() << " test(s):\n";
        
        int counter = 1;
        for (int i = 0; i < m_testProcedures.size(); i++) {
            std:: pair<std::string, TestProcedure> & _test = m_testProcedures[i];
            std::cout << "Test #" << counter << " \"" << _test.first << "\" ";
            ( * _test.second )();
            std::cout << std::endl;
            ++ counter;
        }
        
        std::cout << "Finished running tests.\n";
    }
    
    /*-----------------------------------------------------------------*/
    
private:
    
    /*-----------------------------------------------------------------*/
    
    std::vector< std::pair< std::string, TestProcedure > > m_testProcedures;
    
    /*-----------------------------------------------------------------*/
    
};


/*****************************************************************************/


static TestsRunner gs_TestsRunner;


/*****************************************************************************/


class TestProcedureWrapper
{
public:
    TestProcedureWrapper ( std::string const & _tpName, TestProcedure _tp )
    {
        gs_TestsRunner.addTest( _tpName, _tp );
    }
};


/*****************************************************************************/


#define DECLARE_STA_TEST( arg_testProcedureName )                                                                        \
void arg_testProcedureName ();                                                                                        \
static TestProcedureWrapper gs_wrapper_##arg_testProcedureName( #arg_testProcedureName, & arg_testProcedureName );    \
void arg_testProcedureName ()

/*****************************************************************************/

#define ASSERT_THROWS( TESTED_CODE, EXPECTED_MESSAGE )        \
try                                                     \
{                                                       \
{ TESTED_CODE; }                                    \
assert( ! "Exception must have been thrown" );      \
}                                                       \
catch ( const std::exception & e )                      \
{                                                       \
assert( ! strcmp( e.what(), EXPECTED_MESSAGE ) );   \
}


/*****************************************************************************/

int main ()
{
    gs_TestsRunner.runTests();
}

/*****************************************************************************/

#endif /* TestCase_hpp */
