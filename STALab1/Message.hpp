//
//  Message.hpp
//  STALab1
//
//  Created by trung on 23.06.2018.
//  Copyright © 2018 trung. All rights reserved.
//

#ifndef Message_hpp
#define Message_hpp

#include <stdio.h>

namespace Message {
    const char * const FrontBlockIsFull = "Front block is full";
    const char * const DequeIsEmty = "Deque is empty";
    const char * const FrontBlockIsEmpty = "Front block is empty";
    const char * const BackBlockIsFull = "Back block is full";
    const char * const BackBlockIsEmpty = "Back block is empty";
}

#endif /* Message_hpp */
