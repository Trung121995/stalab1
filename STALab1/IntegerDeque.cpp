//
//  IntegerDeque.cpp
//  STALab1
//
//  Created by trung on 22.06.2018.
//  Copyright © 2018 trung. All rights reserved.
//

#include "IntegerDeque.hpp"
#include "Message.hpp"
#include <stdexcept>

/*****************************************************************************/

void IntegerDequeInit(IntegerDeque & _queue) {
    _queue.m_blockSize = 6;
    _queue.m_frontUsed = 0;
    _queue.m_backUsed = 0;
    _queue.m_directorySize = 5;
    _queue.m_frontBlockIndex = _queue.m_directorySize / 2;
    _queue.m_backBlockIndex = _queue.m_frontBlockIndex + 1;
    
    _queue.m_pDirectory = new int *[_queue.m_blockSize];
    for (int i = 0; i < _queue.m_blockSize; i++) {
        _queue.m_pDirectory[i] = new int [_queue.m_blockSize];
    }
    
    for (int i = 0; i< _queue.m_directorySize; i++) {
        for (int j = 0; j< _queue.m_blockSize; j++) {
            _queue.m_pDirectory[i][j] = 0;
        }
    }
}

/*****************************************************************************/

void IntegerDequeDestroy(IntegerDeque & _queue) {
    for (int i = 0; i< _queue.m_directorySize; i++) {
        delete [] _queue.m_pDirectory[i];
    }
    delete [] _queue.m_pDirectory;
}

/*****************************************************************************/

void IntegerDequeClear(IntegerDeque & _queue) {
    for (int i = 0; i< _queue.m_directorySize; i++) {
        for (int j = 0; j< _queue.m_blockSize; j++) {
            _queue.m_pDirectory[i][j] = 0;
        }
    }
    _queue.m_frontUsed = 0;
    _queue.m_backUsed = 0;
}

/*****************************************************************************/

bool IntegerDequeIsEmpty(const IntegerDeque & _queue) {
    if (_queue.m_frontBlockIndex + 1 != _queue.m_backBlockIndex) {
        return false;
    }
    if (_queue.m_frontUsed != 0 || _queue.m_backUsed != 0) {
        return false;
    }
    return true;
}

/*****************************************************************************/

void IntegerDequePushFront(IntegerDeque & _queue, const int value) {
    if (_queue.m_frontUsed == _queue.m_blockSize) {
        if (_queue.m_frontBlockIndex == 0) {
            throw std::logic_error (Message::FrontBlockIsFull);
        }
        _queue.m_frontUsed = 0;
        _queue.m_frontBlockIndex = (_queue.m_frontBlockIndex - 1);
    }
    _queue.m_frontUsed++;
    _queue.m_pDirectory[_queue.m_frontBlockIndex][_queue.m_frontUsed] = value;
}

/*****************************************************************************/

void IntegerDequePopFront(IntegerDeque & _queue) {
    if (_queue.m_frontUsed == 0) {
        if (_queue.m_frontBlockIndex == _queue.m_backBlockIndex - 1) {
            throw std::logic_error (Message::FrontBlockIsEmpty);
        }
        _queue.m_frontUsed = _queue.m_blockSize;
        _queue.m_frontBlockIndex = (_queue.m_frontBlockIndex + 1);
    }
    _queue.m_pDirectory[_queue.m_frontBlockIndex][_queue.m_frontUsed] = 0;
    _queue.m_frontUsed --;
}

/*****************************************************************************/

int IntegerDequeFrontValue(const IntegerDeque & _queue) {
    if ((_queue.m_frontBlockIndex == _queue.m_backBlockIndex - 1) && (_queue.m_frontUsed == 0)) {
        throw std::logic_error (Message::FrontBlockIsEmpty);
    }
    return _queue.m_pDirectory[_queue.m_frontBlockIndex][_queue.m_frontUsed];
}

/*****************************************************************************/

void IntegerDequePushBack(IntegerDeque & _queue, const int value) {
    if (_queue.m_backUsed == _queue.m_blockSize) {
        if (_queue.m_backBlockIndex == _queue.m_directorySize) {
            throw std::logic_error (Message::BackBlockIsFull);
        }
        _queue.m_backUsed = 0;
        _queue.m_backBlockIndex = (_queue.m_backBlockIndex + 1);
    }
    _queue.m_backUsed++;
    _queue.m_pDirectory[_queue.m_backBlockIndex][_queue.m_backUsed] = value;
}

/*****************************************************************************/

void IntegerDequePopBack(IntegerDeque & _queue) {
    if (_queue.m_backUsed == 0) {
        if (_queue.m_backBlockIndex == _queue.m_frontBlockIndex + 1) {
            throw std::logic_error (Message::BackBlockIsEmpty);
        }
        _queue.m_backUsed = _queue.m_blockSize;
        _queue.m_backBlockIndex = (_queue.m_backBlockIndex + 1);
    }
    _queue.m_pDirectory[_queue.m_backBlockIndex][_queue.m_backUsed] = 0;
    _queue.m_backUsed --;
}

/*****************************************************************************/

int IntegerDequeBackValue(const IntegerDeque & _queue) {
    if ((_queue.m_backBlockIndex == _queue.m_frontBlockIndex + 1) && (_queue.m_backUsed == 0)) {
        throw std::logic_error (Message::BackBlockIsEmpty);
    }
    return _queue.m_pDirectory[_queue.m_backBlockIndex][_queue.m_backUsed];
}

/*****************************************************************************/

