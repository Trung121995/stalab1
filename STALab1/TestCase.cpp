//
//  TestCase.cpp
//  STALab1
//
//  Created by trung on 22.06.2018.
//  Copyright © 2018 trung. All rights reserved.
//

#include "TestCase.hpp"
#include "IntegerDeque.hpp"
#include "Message.hpp"

/*****************************************************************************/

DECLARE_STA_TEST( integer_deque_test_init ) {
    IntegerDeque pQueue;
    
    IntegerDequeInit(pQueue);
    
    assert(pQueue.m_directorySize == 5);
    assert(pQueue.m_blockSize == 6);
    assert(pQueue.m_frontBlockIndex == pQueue.m_directorySize / 2);
    assert(pQueue.m_backBlockIndex == pQueue.m_frontBlockIndex + 1);
    assert(pQueue.m_frontUsed == 0);
    assert(pQueue.m_backUsed == 0);
    assert(pQueue.m_pDirectory != nullptr);
    
    for (int i = 0 ; i< pQueue.m_directorySize; i++) {
        for (int j = 0; j < pQueue.m_blockSize; j++) {
            assert(pQueue.m_pDirectory[i][j] == 0);
        }
    }
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_clear) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    IntegerDequeClear(pQueue);
    
    assert(pQueue.m_frontUsed == 0);
    assert(pQueue.m_backUsed == 0);
    assert(pQueue.m_pDirectory != nullptr);
    assert(pQueue.m_frontBlockIndex == pQueue.m_directorySize / 2);
    assert(pQueue.m_backBlockIndex == pQueue.m_frontBlockIndex + 1);
    
    for (int i = 0; i< pQueue.m_directorySize; i++) {
        assert(pQueue.m_pDirectory[i] != nullptr);
        for (int j = 0; j< pQueue.m_blockSize; j++) {
            assert(pQueue.m_pDirectory[i][j] == 0);
        }
    }
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_isEmty) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    bool isEmpty = IntegerDequeIsEmpty(pQueue);
    assert(isEmpty);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_empty_with_push) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    IntegerDequePushFront(pQueue, 3);
    bool isEmpty = IntegerDequeIsEmpty(pQueue);
    
    assert(!isEmpty);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_push_front) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectedFrontIndex = pQueue.m_frontBlockIndex;
    int expectedFrontUsed = pQueue.m_blockSize;
    
    for (int i = 0; i< pQueue.m_blockSize; i++) {
        IntegerDequePushFront(pQueue, i);
    }
    assert(pQueue.m_frontUsed == expectedFrontUsed);
    assert(pQueue.m_frontBlockIndex == expectedFrontIndex);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_push_front_next_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectedFrontIndex = pQueue.m_frontBlockIndex - 1;
    
    for (int i = 0; i< pQueue.m_blockSize + 1; i++) {
        IntegerDequePushFront(pQueue, i);
    }
    
    assert(pQueue.m_frontUsed == 1);
    assert(pQueue.m_frontBlockIndex == expectedFrontIndex);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_push_front_end_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    int frontBlock = pQueue.m_frontBlockIndex;
    for (int i = 0; i<= frontBlock; i++) {
        for (int j = 0; j< pQueue.m_blockSize; j++) {
            IntegerDequePushFront(pQueue, i + j);
        }
    }
    
    ASSERT_THROWS(IntegerDequePushFront(pQueue, 2), Message::FrontBlockIsFull);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_pop_front) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectedFrontBlockIndex = pQueue.m_frontBlockIndex;
    
    for (int i = 0 ; i< pQueue.m_blockSize; i++) {
        IntegerDequePushFront(pQueue, i);
    }
    IntegerDequePopFront(pQueue);
    
    assert(pQueue.m_frontUsed == pQueue.m_blockSize - 1);
    assert(pQueue.m_frontBlockIndex == expectedFrontBlockIndex);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_pop_previous_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectedFrontBlockIndex = pQueue.m_frontBlockIndex;
    
    for (int i = 0; i< pQueue.m_blockSize + 1; i++) {
        IntegerDequePushFront(pQueue, i);
    }
    IntegerDequePopFront(pQueue);
    IntegerDequePopFront(pQueue);
    
    assert(pQueue.m_frontBlockIndex == expectedFrontBlockIndex);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_pop_end_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    ASSERT_THROWS(IntegerDequePopFront(pQueue), Message::FrontBlockIsEmpty);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_front_value_empty) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    ASSERT_THROWS(IntegerDequeFrontValue(pQueue), Message::FrontBlockIsEmpty);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_front_value_with_push) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectFrontValue = 2;
    
    IntegerDequePushFront(pQueue, 2);
    
    assert(IntegerDequeFrontValue(pQueue) == expectFrontValue);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_front_value_with_next_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectFrontValue = 3;
    
    for(int i = 0; i< pQueue.m_blockSize; i++) {
        IntegerDequePushFront(pQueue, i);
    }
    IntegerDequePushFront(pQueue, 3);
    
    assert(IntegerDequeFrontValue(pQueue) == expectFrontValue);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_front_value_with_push_and_pop) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectedValue = 4;
    
    for(int i = 0; i< pQueue.m_blockSize + 1; i++) {
        IntegerDequePushFront(pQueue, i);
    }
    IntegerDequePushFront(pQueue, 5);
    IntegerDequePopFront(pQueue);
    IntegerDequePushFront(pQueue, expectedValue);
    
    assert(IntegerDequeFrontValue(pQueue) == expectedValue);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_test_push_back) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectBackIndex = pQueue.m_backBlockIndex;
    
    for(int i = 0; i< pQueue.m_blockSize - 1; i++) {
        IntegerDequePushBack(pQueue, i);
    }
    
    assert(pQueue.m_backBlockIndex == expectBackIndex);
    assert(pQueue.m_backUsed == pQueue.m_blockSize - 1);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_push_back_next_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectBackIndex = pQueue.m_backBlockIndex + 1;
    
    for(int i = 0; i < pQueue.m_blockSize + 1; i++) {
        IntegerDequePushBack(pQueue, i);
    }
    
    assert(pQueue.m_backBlockIndex == expectBackIndex);
    assert(pQueue.m_backUsed == 1);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_push_back_out_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    for (int i = pQueue.m_backBlockIndex; i<= pQueue.m_directorySize; i++) {
        for (int j = 0; j < pQueue.m_blockSize; j++) {
            IntegerDequePushBack(pQueue, i + j);
        }
    }
    
    ASSERT_THROWS(IntegerDequePushBack(pQueue, 2), Message::BackBlockIsFull);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_pop_back_is_empty) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    ASSERT_THROWS(IntegerDequePopBack(pQueue), Message::BackBlockIsEmpty);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_pop_back) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectBackBlockIndex = pQueue.m_backBlockIndex;
    
    for(int i = 0; i< pQueue.m_blockSize; i++) {
        IntegerDequePushBack(pQueue, i);
    }
    IntegerDequePopBack(pQueue);
    
    assert(pQueue.m_backBlockIndex == expectBackBlockIndex);
    assert(pQueue.m_backUsed == (pQueue.m_blockSize - 1));
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_pop_back_with_front_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectBackBlockIndex = pQueue.m_backBlockIndex + 1;
    
    for (int i = 0 ; i< pQueue.m_blockSize + 2; i++) {
        IntegerDequePushBack(pQueue, i);
    }
    IntegerDequePopBack(pQueue);
    
    assert(pQueue.m_backBlockIndex == expectBackBlockIndex);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_back_value_empty) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    
    ASSERT_THROWS(IntegerDequeBackValue(pQueue), Message::BackBlockIsEmpty);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_back_value_with_push) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectedBackValue = 2;
    
    IntegerDequePushBack(pQueue, 2);
    
    assert(IntegerDequeBackValue(pQueue) == expectedBackValue);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_back_value_with_next_index) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectBackValue = 3;
    
    for(int i = 0; i< pQueue.m_blockSize; i++) {
        IntegerDequePushBack(pQueue, i);
    }
    IntegerDequePushBack(pQueue, 3);
    
    assert(IntegerDequeBackValue(pQueue) == expectBackValue);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/

DECLARE_STA_TEST(integer_deque_push_and_pop_back_value) {
    IntegerDeque pQueue;
    IntegerDequeInit(pQueue);
    int expectedValue = 4;
    
    for(int i = 0; i< pQueue.m_blockSize + 1; i++) {
        IntegerDequePushBack(pQueue, i);
    }
    IntegerDequePushBack(pQueue, 5);
    IntegerDequePopBack(pQueue);
    IntegerDequePushBack(pQueue, 4);
    
    assert(IntegerDequeBackValue(pQueue) == expectedValue);
    
    IntegerDequeDestroy(pQueue);
}

/*****************************************************************************/
